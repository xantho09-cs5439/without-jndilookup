If you're not here because of CS5439 (2023/24), go away.

The `log4j-core-2.14.1.jar` file has the `JndiLookup.class` file surgically ripped out. It can't perform JNDI 
Lookups even if it wanted to.
