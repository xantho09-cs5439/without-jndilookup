import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    protected static final Logger logger = LogManager.getLogger();
    public static void main(String[] args) {
        logger.info("Lookups can still occur: ${java:version}");
        logger.info("But JNDI Lookups just... don't: ${jndi:ldap://localhost:1389/a}");
    }
}
